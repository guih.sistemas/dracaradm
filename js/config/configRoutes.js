angular.module("dracarAdm").config(function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home', {
            url: "/home",
            controller: "mainController",
            templateUrl: "view/home.html"
        })

        // BEGIN USUARIO
        .state('usuarios', {
            url: "/usuarios",
            controller: "mainController",
            templateUrl: "view/usuarios.html"
        })
        .state('usuarioEditar', {
            url: "/usuarioEditar/:id/:retorno/:placa/:mes/:ano",
            controller: "mainController",
            templateUrl: "view/usuario-editar.html",
        })
        .state('relatorioUsuario', {
            url: "/relatorioUsuario",
            controller: "mainController",
            templateUrl: "view/relatorio-usuario.html"
        })
        // END   USUARIO

        // BEGIN LOCADORA
        .state('locadoras', {
            url: "/locadoras",
            controller: "mainController",
            templateUrl: "view/locadoras.html"
        })
        .state('locadoraNova', {
            url: "/locadoraNova",
            controller: "mainController",
            templateUrl: "view/locadora-nova.html"
        })
        .state('locadoraEditar', {
            url: "/locadoraEditar/:id",
            controller: "mainController",
            templateUrl: "view/locadora-editar.html"
        })
        .state('relatorioLocadora', {
            url: "/relatorioLocadora",
            controller: "mainController",
            templateUrl: "view/relatorio-locadora.html"
        })
        // END   LOCADORA

        // BEGIN VEICULO
        .state('veiculos', {
            url: "/veiculos/:idLocadora",
            controller: "mainController",
            templateUrl: "view/veiculos.html"
        })
        .state('veiculoEditar', {
            url: "/veiculoEditar/:id/:idLocadora",
            controller: "mainController",
            templateUrl: "view/veiculo-editar.html"
        })
        .state('veiculoNovo', {
            url: "/veiculoNovo/:idLocadora",
            controller: "mainController",
            templateUrl: "view/veiculo-novo.html"
        })
        .state('relatorioVeiculo', {
            url: "/relatorioVeiculo/:placa/:mes/:ano",
            controller: "mainController",
            templateUrl: "view/relatorio-veiculo.html"
        })
        .state('despesa', {
            url: "/despesa",
            controller: "mainController",
            templateUrl: "view/despesa.html"
        })
        // END   VEICULO

        // BEGIN LOCACOES
        .state('locacoes', {
            url: "/locacoes",
            controller: "mainController",
            templateUrl: "view/locacoes.html"
        })
        // END   LOCACOES



});
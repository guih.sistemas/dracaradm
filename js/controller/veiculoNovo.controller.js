angular.module("dracarAdm").controller("veiculoNovoController", function ($scope, $http, $state, $stateParams, veiculoFactory) {
    var vm = this;
    vm.loading = true;
    vm.idLocadora = $stateParams.idLocadora;
    vm.check = true;
    
    vm.add = function () {
        if (vm.foto != undefined || vm.foto != null) {
            vm.veiculo.foto = vm.foto.substring(vm.foto.indexOf(',') + 1);
        }
        vm.veiculo.ativo = vm.check;
        vm.veiculo.idLocadora = vm.idLocadora;
        veiculoFactory.add(vm.veiculo).then(function (response) {
            $state.go("veiculos", { idLocadora: vm.idLocadora });
        }, function (data) { alert(response.data.message); });
    };
    vm.cancelar = function () {
        $state.go("veiculos", { idLocadora: vm.idLocadora });
    };
    $('input[type="file"]').change(function (e) {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            vm.foto = reader.result;
            $("#img").attr('src', vm.foto);
        };
    });

    // Init
    var init = function () {
    };
    init();

    $('#valorMensal').maskMoney();
    $('#valorDiaria').maskMoney();
    $('#valorSemanal').maskMoney();
});
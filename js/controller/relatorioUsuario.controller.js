angular.module("dracarAdm").controller("relatorioUsuarioController", function ($scope, usuarioFactory) {
    var vm = this;
    vm.loading = true;
    vm.showDropLocadora = true;

    vm.getAll = function () {
        usuarioFactory.getAll().then(function (response) {
            vm.loading = false;
        }, function (data) { alert(response.data.message); });

    };

    // Init
    var init = function () {
        
    };
    init();
});
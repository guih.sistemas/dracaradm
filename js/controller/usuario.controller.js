angular.module("dracarAdm").controller("usuarioController", function ($scope, $http, usuarioFactory) {
    var vm = this;
    vm.loading = true;
    
    vm.getAll = function () {
        usuarioFactory.getAll().then(function (response) {
            vm.usuario = [];
            var statusNome = "";
            for (var i = 0; i < response.data.dataList.length; i++) {

                statusNome = response.data.dataList[i].status == 1 ? "Cadastro Básico"
                    : response.data.dataList[i].status == 2 ? "Cadastro Sem Documento"
                        : response.data.dataList[i].status == 3 ? "Analise"
                            : response.data.dataList[i].status == 4 ? "Aprovado"
                                : "Reprovado";

                vm.usuario.push(
                    [
                        response.data.dataList[i].id,
                        response.data.dataList[i].nome,
                        response.data.dataList[i].cpf,
                        statusNome,
                        "<a href='#!/usuarioEditar/" + response.data.dataList[i].id + "/usuarios/0/0/0'><i class='ft-edit'></i></a>",
                        "<a href='https://web.whatsapp.com/send?phone=55" + response.data.dataList[i].celular + "' target='_blank'><i class='fa fa-whatsapp'></i></a>"
                    ]);

            }
            $('#usuariosTable').DataTable({
                data: vm.usuario,
                processing: true,
                columns: [
                    { title: "#" },
                    { title: "Nome" },
                    { title: "Cpf" },
                    { title: "Status" },
                    { title: "Editar" },
                    { title: "Contato" }
                ]
            });
            vm.loading = false;
        }, function (data) { alert(response.data.message); });

    };

    // Init
    var init = function () {        
        vm.getAll();
    };
    init();
});
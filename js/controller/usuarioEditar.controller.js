angular.module("dracarAdm").controller("usuarioEditarController", function ($state, $stateParams, usuarioFactory) {
    var vm = this;
    vm.id = $stateParams.id;
    vm.retorno = $stateParams.retorno;
    vm.placa = $stateParams.placa;
    vm.mes = $stateParams.mes;
    vm.ano = $stateParams.ano;
    vm.usuario = null;
    vm.check = false;
    vm.statusSalvo = false;


    vm.getUser = function () {
        usuarioFactory.getUser(vm.id).then(function (response) {
            vm.usuario = response.data.data;
            vm.statusSalvo = vm.usuario.status == 4;

            vm.usuario.statusNome = vm.usuario.status == 1 ? "Cadastro Básico"
                : vm.usuario.status == 2 ? "Cadastro Sem Documento"
                    : vm.usuario.status == 3 ? "Analise"
                        : vm.usuario.status == 4 ? "Aprovado"
                            : "Reprovado";

            vm.check = vm.usuario.status == 4;
        }, function (data) { alert(response.data.message); });
    };
    vm.salvar = function () {
        usuarioFactory.salvarAprovacao(vm.usuario.id, vm.check, vm.usuario.motivo).then(function (response) {
            if(vm.ano != undefined || vm.ano != null || vm.ano > 0 ){
                $state.go(vm.retorno, {placa: vm.placa, mes: vm.mes, ano: vm.ano});
            }
            else{
                $state.go(vm.retorno);
            }
        }, function (data) { alert(response.data.message); });
    };
    vm.cancelar = function () {
        if(vm.ano != undefined || vm.ano != null || vm.ano > 0 ){
            $state.go(vm.retorno, {placa: vm.placa, mes: vm.mes, ano: vm.ano});
        }
        else{
            $state.go(vm.retorno);
        }
    };


    // Init
    var init = function () {
        vm.getUser();
    };
    init();
});
angular.module("dracarAdm").controller("veiculoEditarController", function ($scope, $http, $state, $stateParams, veiculoFactory) {
    var vm = this;
    vm.id = $stateParams.id;
    vm.idLocadora = $stateParams.idLocadora;
    vm.veiculo = null;
    vm.foto = null;
    vm.check = false;

    vm.getveiculo = function () {
        veiculoFactory.getveiculo(vm.id).then(function (response) {
            vm.veiculo = response.data.dataSite;
            vm.check = response.data.dataSite.ativo;
        }, function (data) { alert(response.data.message); });
    };
    vm.atualizar = function () {
        if (vm.foto != undefined || vm.foto != null) {
            vm.veiculo.foto = vm.foto.substring(vm.foto.indexOf(',') + 1);
        }
        vm.veiculo.ativo = vm.check;
        veiculoFactory.atualizar(vm.veiculo).then(function (response) {
            $state.go("veiculos", {idLocadora: vm.idLocadora});
        }, function (data) { alert(response.data.message); });
    };
    vm.cancelar = function () {
        $state.go("veiculos", {idLocadora: vm.idLocadora});
    };
    $('input[type="file"]').change(function (e) {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            vm.foto = reader.result;
            $("#img").attr('src', vm.foto);
        };
    });
    // Init
    var init = function () {
        vm.getveiculo();
    };
    init();

    $('#valorMensal').maskMoney();
    $('#valorDiaria').maskMoney();
    $('#valorSemanal').maskMoney();
});
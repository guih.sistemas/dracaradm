angular.module("dracarAdm").controller("locadoraEditarController", function ($scope, $http, $state, $stateParams, locadoraFactory) {
    var vm = this;
    vm.id = $stateParams.id;
    vm.locadora = null;
    vm.foto = null;
    vm.check = false;

    vm.getLocadora = function () {
        locadoraFactory.getLocadora(vm.id).then(function (response) {
            vm.locadora = response.data.data;
            vm.check = response.data.data.ativo;
        }, function (data) { alert(response.data.message); });
    };
    vm.atualizar = function () {
        if (vm.foto != undefined || vm.foto != null) {
            vm.locadora.foto = vm.foto.substring(vm.foto.indexOf(',') + 1);
        }
        vm.locadora.ativo = vm.check;
        locadoraFactory.atualizar(vm.locadora).then(function (response) {
            $state.go("locadoras");
        }, function (data) { alert(response.data.message); });
    };
    vm.cancelar = function () {
        $state.go("locadoras");
    };
    $('input[type="file"]').change(function (e) {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            vm.foto = reader.result;
            $("#img").attr('src', vm.foto);
        };
    });
    // Init
    var init = function () {
        vm.getLocadora();
    };
    init();
});
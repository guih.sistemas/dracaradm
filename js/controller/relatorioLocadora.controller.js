angular.module("dracarAdm").controller("relatorioLocadoraController", function ($scope, locadoraFactory) {
    var vm = this;
    vm.loading = false;
    vm.show = false;
    vm.idLocadora = 0;
    vm.dadosRelatorio = [];
    vm.quadroResumo = 0;
    vm.qtdMesesChart = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];





    vm.showing = function (loading, show) {
        vm.loading = loading;
        vm.show = show;
    };
    $('#dropLocadora').change(function (e) {
        vm.dadosRelatorio = [];
        vm.quadroResumo = [];
        vm.fillTable();

        vm.idLocadora = $("#dropLocadora option:selected").val();
        vm.showing(true, false);
        vm.getStatusVeiculos();
    });
    vm.getStatusVeiculos = function () {
        vm.showing(false, false);

        locadoraFactory.getStatusVeiculos(vm.idLocadora).then(function (response) {
            if (response.data.data.length > 0) {
                vm.dadosRelatorio = response.data.data;
                vm.quadroResumo = response.data.quadroResumo;
                vm.fillQtdMesesChart(response.data.graficoMensal);
                vm.fillCharts();
                vm.fillTable();
                vm.showing(false, true);
            }

        }, function (data) { alert(response.data.message); });
    };
    vm.fillQtdMesesChart = function (graficoMensal) {
        vm.qtdMesesChart = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < graficoMensal.length; i++) {
            var mes = graficoMensal[i].mes;
            vm.qtdMesesChart[mes - 1] = graficoMensal[i].qtd;
        }
    };
    vm.fillCharts = function (id) {
        for (var i = 1; i <= 4; i++) {
            new Chartist.Line('#Widget-line-chart' + i, {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
                series: [
                    [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
                ]
            }, {
                axisX: {
                    showGrid: true,
                    showLabel: false,
                    offset: 0,
                },
                axisY: {
                    showGrid: false,
                    low: 40,
                    showLabel: false,
                    offset: 0,
                },
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                fullWidth: true,
            });
        }

        new Chartist.Line('#line-chart1', {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            series: [
                vm.qtdMesesChart
            ]
        }, {
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            fullWidth: true,
            chartPadding: { top: 0, right: 25, bottom: 0, left: 0 }
        });
    };
    vm.fillTable = function () {
        var d = new Date(),
            mes = d.getMonth() + 1,
            ano = d.getFullYear();

        var veiculoStatusTableDados = vm.dadosRelatorio.map(d => {
            var status = d.status == 1 ? "Processando"
                : d.status == 2 ? "Retirar"
                    : d.status == 3 ? "Alugado"
                        : "Disponível";

            var dado = [
                d.id,
                d.marca + '/' + d.modelo,
                d.placa,
                status,
                "<a href='#!/relatorioVeiculo/" + d.placa + "/" + mes + "/" + ano + "'><i class='ft-edit'></i></a>",
            ];

            return dado;
        });

        $("#veiculoStatusTable_wrapper").remove();

        if (veiculoStatusTableDados.length > 0) {
            var t = "<table id='veiculoStatusTable' style='margin: 0 auto 2em auto;' class='table table-striped table-bordered'></table>";
            $("#tableTarget").append(t);

            var table = $('#veiculoStatusTable');
            table.DataTable({
                data: veiculoStatusTableDados,
                processing: true,
                columns: [

                    { title: "#" },
                    { title: "Marca/Modelo" },
                    { title: "Placa" },
                    { title: "Status" },
                    { title: "Histórico" }
                ],
            });
        }
    };

    $(document).ready(function () {
        if ($("#dropLocadora option:selected").val() != "0") {
            vm.dadosRelatorio = [];
            vm.quadroResumo = [];
            vm.fillTable();

            vm.idLocadora = $("#dropLocadora option:selected").val();
            vm.showing(true, false);
            vm.getStatusVeiculos();
        }
    });

    // Init
    var init = function () {
    };
    init();
});
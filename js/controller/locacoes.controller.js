angular.module("dracarAdm").controller("locacoesController", function ($scope, $http, $state, $stateParams, locacoesFactory) {
    var vm = this;
    vm.loading = true;
    vm.idLocadora = $stateParams.idLocadora;

    vm.getAll = function () {
        veiculoFactory.getAll(vm.idLocadora).then(function (response) {
            vm.veiculo = [];
            for (var i = 0; i < response.data.data.length; i++) {
                var ativoNome = response.data.data[i].ativo ? "Ativo" : "Inativo";
                vm.veiculo.push(
                    [
                        response.data.data[i].id,
                        response.data.data[i].marca + "/" + response.data.data[i].modelo,
                        response.data.data[i].placa,
                        response.data.data[i].anoFabricacao + "/" + response.data.data[i].anoModelo,
                        ativoNome,
                        "<a href='#!/veiculoEditar/" + response.data.data[i].id + "/" + vm.idLocadora + "' ui-sref='veiculoEditar({id:" + response.data.data[i].id + "/" + vm.idLocadora + "})'><i class='ft-edit'></i></a>"
                    ]);
            }

            var table = $('#veiculosTable');
            table.DataTable({
                data: vm.veiculo,
                processing: true,
                columns: [
                    
                    { title: "#"},
                    { title: "Marca/Modelo" },
                    { title: "Placa" },
                    { title: "Ano" },
                    { title: "Status" },
                    { title: "Editar" }
                ],
            });
            vm.loading = false;
        }, function (data) { alert(response.data.message); });
    };
    vm.novo = function () {
        $state.go("veiculoNovo", {idLocadora: vm.idLocadora});
    };

    // Init
    var init = function () {
    };
    init();
});
angular.module("dracarAdm").controller("despesaController", function ($scope, $stateParams, veiculoFactory) {
    var vm = this;    
    vm.loading = true;
    vm.tipo = { id: 1, tipo: "Locação" };
    vm.valor = "0,00";
    vm.placa = "";
    vm.tipos = [
        { id: 1, tipo: "Locação" },
        { id: 2, tipo: "Renovação" },
        { id: 3, tipo: "Sinistro" },
        { id: 4, tipo: "Alteração" },
        { id: 5, tipo: "Cancelamento" }       
    ];

    vm.getVeiculo = function () {
        veiculoFactory.get(vm.placa).then(function (response) {
            vm.veiculo = response.data.data;
            vm.loading = false;
        }, function (data) { alert(response.data.message); });
    };

    // Init
    var init = function () {
    };
    init();

    $('#valor').maskMoney();
});
angular.module("dracarAdm").controller("relatorioVeiculoController", function ($scope, $stateParams, veiculoFactory) {
    var vm = this;    
    vm.loading = true;
    vm.meses = [
        { id: 1, mes: "Janeiro" },
        { id: 2, mes: "Fevereiro" },
        { id: 3, mes: "Março" },
        { id: 4, mes: "Abril" },
        { id: 5, mes: "Maio" },
        { id: 6, mes: "Junho" },
        { id: 7, mes: "Julho" },
        { id: 8, mes: "Agosto" },
        { id: 9, mes: "Setempro" },
        { id: 10, mes: "Outubro" },
        { id: 11, mes: "Novembro" },
        { id: 12, mes: "Dezembro" }        
    ];
    vm.placa = $stateParams.placa == 0 ? "" : $stateParams.placa;
    vm.mes = vm.meses[$stateParams.mes - 1];
    vm.ano = $stateParams.ano;
    vm.showDropLocadora = true;
    vm.historico = [];
    vm.filtro = {placa: vm.placa, mes: vm.mes, ano: vm.ano};


    vm.getHistorico = function () {
        vm.filtro.placa = vm.placa;
        vm.filtro.mes = vm.mes;
        vm.filtro.ano = vm.ano;

        veiculoFactory.getHistorico(vm.placa, vm.mes.id, vm.ano).then(function (response) {
            vm.historico = response.data.data;
            vm.loading = false;
        }, function (data) { alert(response.data.message); });

    };

    // Init
    var init = function () {
        vm.getHistorico();
    };
    init();
});
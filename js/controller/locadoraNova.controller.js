angular.module("dracarAdm").controller("locadoraNovaController", function ($scope, $http, $state, locadoraFactory) {
    var vm = this;
    vm.check = true;

    vm.add = function () {
        if (vm.foto != undefined || vm.foto != null) {
            vm.locadora.foto = vm.foto.substring(vm.foto.indexOf(',') + 1);
        }
        vm.locadora.ativo = vm.check;
        locadoraFactory.add(vm.locadora).then(function (response) {
            $state.go("locadoras");
        }, function (data) { alert(response.data.message); });
    };
    vm.cancelar = function () {
        $state.go("locadoras");
    };
    $('input[type="file"]').change(function (e) {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            vm.foto = reader.result;
            $("#img").attr('src', vm.foto);
        };
    });

    // Init
    var init = function () {
    };
    init();
});
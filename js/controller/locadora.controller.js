angular.module("dracarAdm").controller("locadoraController", function ($scope, $http, $state, locadoraFactory) {
    var vm = this;
    vm.loading = true;

    vm.getAll = function () {
        locadoraFactory.getAll().then(function (response) {
            vm.locadora = [];

            for (var i = 0; i < response.data.dataList.length; i++) {
                var ativoNome = response.data.dataList[i].ativo ? "Ativo" : "Inativo";
                vm.locadora.push(
                    [
                        response.data.dataList[i].id,
                        response.data.dataList[i].razaoSocial,
                        response.data.dataList[i].cnpj,
                        ativoNome,
                        "<a href='#!/veiculos/" + response.data.dataList[i].id + "' ui-sref='veiculos({id:" + response.data.dataList[i].id + "})'><i class='ft-list'></i></a>",
                        "<a href='#!/locadoraEditar/" + response.data.dataList[i].id + "' ui-sref='locadoraEditar({id:" + response.data.dataList[i].id + "})'><i class='ft-edit'></i></a>"
                    ]);
            }

            var table = $('#locadorasTable');
            table.DataTable({
                data: vm.locadora,
                processing: true,
                columns: [
                    
                    { title: "#"},
                    { title: "Razão Social" },
                    { title: "Cnpj" },
                    { title: "Status" },
                    { title: "Veículos" },
                    { title: "Editar" }
                ],
            });
            vm.loading = false;
        }, function (data) { alert(response.data.message); });
    };
    vm.nova = function () {
        $state.go("locadoraNova");
    };

    // Init
    var init = function () {
        vm.getAll();
    };
    init();
});
angular.module("dracarAdm").factory("usuarioFactory", function ($http, $q, $httpParamSerializerJQLike) {
    var service = {};
    var url = 'http://www.brflag.com.br/';

    service.getAll = function () {
        var deferred = $q.defer();
        $http(
            {
                method: 'GET',
                url: url + 'api/usuario/get',
                data: ""
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.getUser = function (id) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/usuario/get',
                data: {
                    id: id
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.salvarAprovacao = function (idUsuario, aprovado, motivo) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/usuario/aprovar',
                data: {
                    idUsuario: idUsuario,
                    aprovar: aprovado,
                    motivo: motivo
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }


    return service;
});

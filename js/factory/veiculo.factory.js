angular.module("dracarAdm").factory("veiculoFactory", function ($http, $q, $httpParamSerializerJQLike) {
    var service = {};
    var url = 'http://www.brflag.com.br/';
    

    service.getAll = function (idLocadora) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/veiculo/get',
                data: {
                    idLocadora: idLocadora
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.getHistorico = function (placa, mes, ano) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/veiculo/getHistorico',
                data: {
                    placa: placa,
                    mes: mes,
                    ano: ano
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.getveiculo = function (id) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/veiculo/getVeiculo',
                data: {
                    id: id
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.atualizar = function (veiculo) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/veiculo/atualizar',
                data: {
                    id: veiculo.id,
                    idLocadora : veiculo.idLocadora,
                    placa : veiculo.placa,
                    marca : veiculo.marca,
                    modelo : veiculo.modelo,
                    anoFabricacao : veiculo.anoFabricacao,
                    anoModelo : veiculo.anoModelo,
                    valorDiaria : veiculo.valorDiaria,
                    valorSemanal : veiculo.valorSemanal,
                    valorMensal : veiculo.valorMensal,
                    renavam : veiculo.renavam,
                    foto : veiculo.foto,
                    ativo : veiculo.ativo
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.add = function (veiculo) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/veiculo/add',
                data: {
                    idLocadora : veiculo.idLocadora,
                    placa : veiculo.placa,
                    marca : veiculo.marca,
                    modelo : veiculo.modelo,
                    anoFabricacao : veiculo.anoFabricacao,
                    anoModelo : veiculo.anoModelo,
                    valorDiaria : veiculo.valorDiaria,
                    valorSemanal : veiculo.valorSemanal,
                    valorMensal : veiculo.valorMensal,
                    renavam : veiculo.renavam,
                    foto : veiculo.foto,
                    ativo : veiculo.ativo
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }

    return service;
});

angular.module("dracarAdm").factory("locadoraFactory", function ($http, $q, $httpParamSerializerJQLike) {
    var service = {};
    var url = 'http://www.brflag.com.br/';

    service.getAll = function () {
        var deferred = $q.defer();
        $http(
            {
                method: 'GET',
                url: url + 'api/locadora/get',
                data: ""
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.getLocadora = function (idLocadora) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/locadora/getLocadora',
                data: {
                    id: idLocadora
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.getStatusVeiculos = function (idLocadora) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/locadora/getStatusVeiculos',
                data: {
                    id: idLocadora
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.atualizar = function (locadora) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/locadora/atualizar',
                data: {
                    id: locadora.id,
                    razaoSocial: locadora.razaoSocial,
                    nomeFantasia : locadora.nomeFantasia,
                    cnpj : locadora.cnpj,
                    endereco : locadora.endereco,
                    telefone : locadora.telefone,
                    email : locadora.email,
                    whatsapp : locadora.whatsapp,
                    nomeResponsavel : locadora.nomeResponsavel,
                    latitude : locadora.latitude,
                    longitude : locadora.longitude,
                    foto : locadora.foto,
                    ativo: locadora.ativo
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }
    service.add = function (locadora) {
        var deferred = $q.defer();
        $http(
            {
                method: 'POST',
                url: url + 'api/locadora/add',
                data: {
                    razaoSocial: locadora.razaoSocial,
                    nomeFantasia : locadora.nomeFantasia,
                    cnpj : locadora.cnpj,
                    endereco : locadora.endereco,
                    telefone : locadora.telefone,
                    email : locadora.email,
                    whatsapp : locadora.whatsapp,
                    nomeResponsavel : locadora.nomeResponsavel,
                    latitude : locadora.latitude,
                    longitude : locadora.longitude,
                    foto : locadora.foto,
                    ativo: locadora.ativo
                }
            }).then(function successCallback(response) {
                deferred.resolve(response)
            }, function errorCallback(response) {
                deferred.reject("Error: User not found.")
            });
        return deferred.promise;
    }

    return service;
});
